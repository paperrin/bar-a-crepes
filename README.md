# bar-a-crepes
My take on a job technical test asking to create a Vue.js project including:
* View of different products in list form ( name, description, price, photo... )
* Ability to add products to a shopping cart
* View listing cart's products
* Ability to increase or decrease amount of a specific product in the cart
* Navigation menu with at least 2 pages ( categories, for example main dish and desert )

The design was a plus but it was technique that was analyzed.

## Links
Live Website : http://baracrepes.paulperr.in/

## Screenshots
![Home Page](https://gitlab.com/paperrin/bar-a-crepes/uploads/e44c8e63043fca07071b0e94d8eb8bcc/home.png)
![Product View](https://gitlab.com/paperrin/bar-a-crepes/uploads/39ff9121cb1eb349b68123c4193e8228/product.png)
![Shopping Cart](https://gitlab.com/paperrin/bar-a-crepes/uploads/25449e1510f1decd66ab07f73b15ac15/cart.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
