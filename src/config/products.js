import Product from "../Product.js";

export default [
  {
    name: "Crêpe Abeille",
    price: 4.5,
    description: "Framboises et sauce au miel",
    img: "crepe-framboise-miel.jpg",
    categoryNames: [ "Crêpes", "Sucré" ],
  },
  {
    name: "Crêpe aux Baies",
    price: 5,
    description: "Framboises, myrtilles, coulis fruits rouges",
    img: "crepe-framboise-myrtille.jpg",
    categoryNames: [ "Crêpes", "Sucré" ],
  },
  {
    name: "Crêpe Perchée",
    price: 4.5,
    description: "Pommes, noix, sauce fruits a coque et miel",
    img: "crepe-pomme-noix.jpg",
    categoryNames: [ "Crêpes", "Sucré" ],
  },
  {
    name: "Crêpe au Sucre",
    price: 3,
    description: "Sucre blanc",
    categoryNames: [ "Crêpes", "Sucré" ],
  },
  {
    name: "Crêpe Burger",
    price: 5,
    description: "Steak hache, cheddar, cornichons et sauce burger",
    categoryNames: [ "Crêpes", "Salé" ],
  },
  {
    name: "Milkshake Fraise",
    price: 4,
    description: "Lait, creme glacee a la fraise",
    categoryNames: [ "Milkshakes", "Sucré" ],
  },
  {
    name: "Milkshake Chocolat",
    price: 4,
    description: "Lait, creme glacee au chocolat",
    categoryNames: [ "Milkshakes", "Sucré" ],
  },
  {
    name: "Milkshake Vanille",
    price: 4,
    description: "Lait, creme glacee a la vanille",
    categoryNames: [ "Milkshakes", "Sucré" ],
  },
  {
    name: "Milkshake Pistache",
    price: 4,
    description: "Lait, creme glacee a la pistache",
    categoryNames: [ "Milkshakes", "Sucré" ],
  },
].map(item => new Product(item));
