import shortid from "shortid";

export default [
  { name: "Tout" },
  { name: "Crêpes" },
  { name: "Milkshakes" },
  { name: "Salé" },
  { name: "Sucré" }
].map(category => {
  return {
    ...category,
    id: shortid.generate()
  };
});
