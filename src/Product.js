import shortid from "shortid";
import productCategories from "./config/productCategories.js"

function assertProductError(assertion, product, error) {
  console.assert(assertion, { product, error });
}

export default class {
  constructor(product) {
    let { name, img, price, description, categoryNames } = product;
    categoryNames = categoryNames || [];
    categoryNames = [ productCategories[0].name, ...categoryNames ];
    const categoryIds = categoryNames.map(categoryName => {
      const category = productCategories.find(
        category => category.name === categoryName
      );
      assertProductError(typeof category === "object" && typeof category.id === "string", product, "Invalid product category");
      return category.id;
    });

    description = description || "";

    assertProductError(typeof name === "string", product, "Invalid product name, should be string");
    assertProductError(!img || typeof img === "string", product, "Invalid product imgSrc, should be string or falsy");
    assertProductError(typeof price === "number" && price >= 0, product, "Invalid product price, should be number >= 0");
    assertProductError(typeof description === "string", product, "Invalid product description, should be string");

    this.id = shortid.generate();
    this.name = name;
    this.img = img;
    this.price = price;
    this.description = description;
    this.categoryIds = categoryIds;
  }
}
