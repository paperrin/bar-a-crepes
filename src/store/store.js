import Vuex from "vuex";
import Vue from "vue";
import shortid from "shortid";
import products from "../config/products.js";
import productCategories from "../config/productCategories.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products,
    productCategories,
    cartItems: [],
    openedProductId: null,
    isCartOpen: false,
    selectedCategoryId: productCategories[0].id
  },
  getters: {
    isProductViewOpen: state => state.openedProductId !== null,
    isCartOpen: state => state.isCartOpen,
    openedProductId: state => state.openedProductId,
    getProductById: state => id => state.products.find(product => product.id === id),
    getCartItemByProductId: state => id => state.cartItems.find(item => item.productId === id),
    getCartItemById: state => id => state.cartItems.find(item => item.id === id),
    getProductIdCartQty: state => id => {
      const cartItem = state.cartItems.find(
        item => item.productId === id);
      if (!cartItem)
        return 0;
      return cartItem.qty;
    },
    productCategories: state => state.productCategories,
    selectedCategoryId: state => state.selectedCategoryId,
    products: state => state.products,
    productsOfSelectedCategory: state => {
      return state.products.filter(
        product => {
          return product.categoryIds.includes(state.selectedCategoryId);
        }
      )
    },
    cartItems: state => state.cartItems,
  },
  mutations: {
    setProductViewId(state, productId) {
      state.openedProductId = productId;
    },
    setCartOpen(state, isOpen) {
      state.isCartOpen = isOpen;
    },
    setSelectedCategoryId(state, productCategoryId) {
      state.selectedCategoryId = productCategoryId;
    },
    addCartItem(state, cartItem) {
      state.cartItems.push(cartItem);
    },
    setCartItemQty(state, { cartItem, qty }) {
      cartItem.qty = qty;
    },
    removeCartItemById(state, cartItemId) {
      state.cartItems = state.cartItems.filter(
        item => item.id !== cartItemId
      );
    }
  },
  actions: {
    openProductViewId({ commit }, productId) {
      commit("setProductViewId", productId );
    },
    closeProductView({ commit }) {
      commit("setProductViewId", null);
    },
    openCart({ commit }) {
      commit("setCartOpen", true);
    },
    closeCart({ commit }) {
      commit("setCartOpen", false);
    },
    selectCategoryId({ commit }, productCategoryId) {
      commit("setSelectedCategoryId", productCategoryId);
    },
    setProductIdCartQty({ getters, commit, dispatch }, { productId, qty }) {
      const cartItem = getters.getCartItemByProductId(productId);

      if (!cartItem)
        commit("addCartItem", { id: shortid.generate(), productId, qty });
      else
        dispatch("setCartItemQty", { cartItem, qty });
    },
    addProductIdToCart({ getters, dispatch }, { productId, qty = 1 }) {
      dispatch("setProductIdCartQty", {
        productId,
        qty: getters.getProductIdCartQty(productId) + qty
      });
    },
    setCartItemQty({ commit }, { cartItem, qty }) {
      if (qty > 0)
        commit("setCartItemQty", { cartItem, qty });
      else
        commit("removeCartItemById", cartItem.id)
    }
  }
});
